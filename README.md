[logo]: AKU_1.png


# **EVIL-DOER |** <img src="AKU_1.png"  width="100" height="123"> `Once again, I am free to smite the world as I did in days long past.`

```bash
[Aku@EVIL-DOER ~]$ echo \
> "EVIL-DOER is a program used to define and manage scripts for Sailfish OS using python, dbus-monitor, and timeclient."
```


## Requirements
* Python3; pexpect, dbus
* QT5; timedclient-qt5


## Usage


`Step 0 : Install required packages`
```bash
[nemo@Sailfish ~]$ pip3 install dbus pexpect
```

`Step 1/3 : python3 EVIL-DOER.py --initdb`

```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py --initdb

                Who dares to summon the Master of Masters,
                       the Deliverer of Darkness,
                          the Shogun of Sorrow!
                 

[+] Creating database...
[+] Database created!
```

`Step 2/3 : python3 EVIL-DOER.py --interactiveschedule`


```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py --interactiveschedule

[+] Enter task name (eg Super_Villian_News_Checker)
[NAME] Villiany

[+] Enter path/filename (eg /home/nemo/Documents/test.sh)
[PATHFILE] /home/nemo/Documents/Villiany.sh

[+] Enter every X seconds to run task, when screen is on. (eg 3600)
[SECONDS] 3600

[+] Enter every X seconds to run task, when user unlocks screen. (eg 3600 for every hour or 1 for everytime device is unlocked)
[SECONDS] 3600

[+] Activate script now? (Default=Y)
[Yy/Nn] Y

[+] Scheduled script successfully created!
```


`Step 3/3 : python3 EVIL-DOER.py`

```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py


                 Once again, I am free to smite the world as I did in days long past. 

1 "<--Current screen state"
[+] DBUS Monitor started
```

`Show scripts in database`

```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py --scripts         
[+] Checking database for scripts to run...
[+] Found 1 scripts in database

[Script Name]    Evil_Plan_Updates
[Path + File]    /home/nemo/Documents/Evil_Plan_Updates.sh
[Script_Active]  True
[Description]
 * Script will run every 1800 seconds if screen is on
 * Script will run every 3600 seconds when user unlocks device
```

`Remove a script database`
```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py --removescript Villiany
[+] Checking database for scripts to remove...
[+] Villiany successfully removed from database!
```

`Help`

```bash
[nemo@Sailfish ~]$ python3 EVIL-DOER.py -h
  -h, --help            show this help message and exit
  --scripts [SCRIPTS]   Print scripts in database; Default will print all
                        scripts
  --removescript [REMOVESCRIPT]
                        Remove script from database by name
  --initdb [INITDB]     Initialize database
  --dbrefresh [DBREFRESH]
                        Refresh time.sleep(Seconds) for database (default 900)
  --initscreen [INITSCREEN]
                        Default=0; if SSH to unit set to to 1; elif from unit
                        or inside app set to 0;
  --schedule [SCHEDULE]
                        Schedule a task via CLI in one command (eg --schedule
                        "Name /home/file.sh 3600 3600 1")
  --interactiveschedule [INTERACTIVESCHEDULE]
                        Schedule a task from interactive prompts
  --test [TEST]         Add a test task
  --testnotification [TESTNOTIFICATION]
                        Pop up a test notification
```






