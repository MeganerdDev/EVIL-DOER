#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import re
import time
import pexpect
import threading
import sqlite3
from subprocess import call
import random
import argparse

# DBUS nemo notificationmanager.cpp
# https://git.merproject.org/mer-core/lipstick/blob/e1de788ef095ad71b04207875a54e6399a285af9/src/notifications/notificationmanager.cpp

def MOTD():
    ''' EVIL-DOER author message on program start '''
    Phrases = ['''Once again, I am free to smite the world
                      as I did in days long past.''',
                '''
                Who dares to summon the Master of Masters,
                       the Deliverer of Darkness,
                          the Shogun of Sorrow!
                ''',
                '''
                To all those fools who dare oppose me, your hero is dead!
                      Your hope is dead! I am the one true master!
                        Let this be a lesson to all! Time to die
                ''']
    print('\n'*3, Phrases[random.randint(0, len(Phrases)-1)], '\n'*3)
    return

def Init_DB():
    print('[+] Creating database...')
    
    # Create DB
    conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
    
    c = conn.cursor()
    
    # Create table
    # scriptname; string; Filename
    # scriptfile; string; Path+Filename
    # runevery; int; Will run every N seconds while screen is on; Does not control run on wake
    # runeverywake; int; Will run every N seconds when you wake the phone the next time
    # lastran; int; Epoch time
    c.execute('''CREATE TABLE Scripts (scriptname text, scriptfile text, runevery int, runeverywake int, lastran int, lastwakeran int, active int)''')

    # Insert table data
    Epoch_Time = int(time.time())
    
    # Test scripts
    #c.execute("INSERT INTO Scripts VALUES ('TESTDEVA','/home/nemo/Documents/test.sh',1800,3600,%d,%d,1)" % (Epoch_Time-13600, Epoch_Time-13600))
    #c.execute("INSERT INTO Scripts VALUES ('TESTDEVB','/home/nemo/Documents/test3.sh',1800,3600,%d,%d,1)" % (Epoch_Time, Epoch_Time))
    
    # Alert every time phone unlocked
    #c.execute("INSERT INTO Scripts VALUES ('TESTDEVC','/home/nemo/Documents/test.sh',30,1,%d,%d,1)" % (Epoch_Time-13600, Epoch_Time-13600))

    # Commit changes
    conn.commit()
    
    # Close connection
    conn.close()
    
    print('[+] Database created!')
    sys.exit()

def Delete_Script_From_DB(Script_Name):
    if not os.path.isfile('UNSPEAKABLEEVIL.db'):
        print('[!] Cannot find database. Please restart EVIL-DOER or initialize the database.')
        return []
    
    print('[+] Checking database for scripts to remove...')
    
    conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
    c = conn.cursor()

    # Select script info from database
    c.execute('''DELETE FROM `Scripts` WHERE `scriptname`="%s";''' % Script_Name)
    
    # Commit changes
    conn.commit()
    
    # Close connection
    conn.close()
    
    print('[+] %s successfully removed from database!' % Script_Name)
    
    sys.exit()


def Get_Scripts_From_DB():
    if not os.path.isfile('UNSPEAKABLEEVIL.db'):
        print('[!] Cannot find database. Please restart EVIL-DOER or initialize the database.')
        return []
    
    conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
    c = conn.cursor()

    # Select script info from database
    c.execute('''SELECT * FROM `Scripts`;''')
    
    # [('TESTDEVA', '/home/nemo/Documents/test.sh', 30, 30, 1529980333)]
    Result = c.fetchall()
    
    # Close connection
    conn.close()
    
    #print(Result)
    return Result

def Update_DB_Last_Runtime(Script_Name):
    print('[+] Updating database for %s last runtime...' % Script_Name)
    conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
    c = conn.cursor()
    
    # Update database
    Epoch_Time = int(time.time())
    c.execute('''UPDATE `Scripts` SET `lastran`=%d WHERE `scriptname`="%s";''' % (Epoch_Time, Script_Name))
    
    # Commit changes
    conn.commit()
    
    # Close connection
    conn.close()
    
    print('[+] Database updated!')
    return

def Update_DB_Last_Wake_Runtime(Script_Name):
    print('[+] Updating database for %s last wake runtime...' % Script_Name)
    conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
    c = conn.cursor()
    
    # Update database
    Epoch_Time = int(time.time())
    c.execute('''UPDATE `Scripts` SET `lastwakeran`=%d WHERE `scriptname`="%s";''' % (Epoch_Time, Script_Name))
    
    # Commit changes
    conn.commit()
    
    # Close connection
    conn.close()
    
    print('[+] Database updated!')
    return

def DBUS_Monitor():
    global Screen
    
    # Pre-load data for wake lock
    # TODO: Remove non wakelock scripts from list
    Preload_Scripts = Get_Scripts_From_DB()
    
    print('\n[+] DBUS Monitor started\n')
    
    # stdout DBUS messages
    child = pexpect.spawn('dbus-monitor --session')
    
    while 1:
        # Listen for screen unlock; This is not when the screen wakes
        for i in range(0, 25):
            try:
                child.expect('/com/jolla/lipstick', timeout=None)
                child.expect('/com/jolla/lipstick', timeout=2)
                break # After success break iteration
            except:
                # Timeout error most likely, but seems to be pretty rare
                print('[!] DBUS_Monitor got exception\n')
        if Screen == 1:
            Screen = 0
        elif Screen == 0:
            Screen = 1
            for script in Preload_Scripts:
                #print(script)
                Script_Name = script[0]
                Script_Pathfile = script[1]
                Run_Every_Wake = script[3]
                Epoch_Last_Wake_Run = script[5]
                Script_Active = script[6]
                
                Epoch_Time = int(time.time())
                
                print(Epoch_Last_Wake_Run + Run_Every_Wake, '<', Epoch_Time)
                
                if Epoch_Last_Wake_Run + Run_Every_Wake < Epoch_Time and Screen == 1 and Script_Active == 1:
                    # Command = ['sh', '/home/nemo/Documents/test.sh']
                    Command = ['sh', Script_Pathfile]
                    print('[+] Running %s' % Script_Name)
                    call(Command)
                    
                    # Update database
                    Update_DB_Last_Wake_Runtime(Script_Name)
                    
                    Preload_Scripts = Get_Scripts_From_DB()
                    # TODO: Change iteration to range(0, len(Preload_Scripts))
                    # TODO: Then just update instead of loading from db
            
        print(Screen)

def Start_Thread(Thread):
    ''' Start worker thread
    + Parameter input
    - Thread is function name to be ran in a thread '''
    t = threading.Thread(target=Thread)
    t.start()

if __name__ == '__main__':
    Epoch_Time = int(time.time())
    global Screen; Screen = 0
    DB_Refresh_Time = 900
    
    parser = argparse.ArgumentParser(description='Schedule great evil')
                        
    parser.add_argument('--scripts', required=False, type=str, nargs='?', const='*', help='Print scripts in database; Default will print all scripts')
    
    parser.add_argument('--removescript', required=False, type=str, nargs='?', const='*', help='Remove script from database by name')
    
    parser.add_argument('--initdb', required=False, type=str, nargs='?', const='*', help='Initialize database')
    
    parser.add_argument('--dbrefresh', required=False, type=str, nargs='?', const='*', help='Refresh time.sleep(Seconds) for database (default 900)')

    parser.add_argument('--initscreen', required=False, type=str, nargs='?', const='*', help='Default=0; if SSH to unit set to to 1; elif from unit or inside app set to 0;')
    
    parser.add_argument('--schedule', required=False, type=str, nargs='?', const='*', help='Schedule a task via CLI in one command \
                                                                                            (eg --schedule "Name /home/file.sh 3600 3600 1")')

    parser.add_argument('--interactiveschedule', required=False, type=str, nargs='?', const='*', help='Schedule a task from interactive prompts')
    
    parser.add_argument('--test', required=False, type=str, nargs='?', const='*', help='Add a test task')

    parser.add_argument('--testnotification', required=False, type=str, nargs='?', const='*', help='Pop up a test notification')
    
    args = parser.parse_args()
    #print('\n[*]', args.scripts)
    
    if args.dbrefresh:
        try:
            DB_Refresh_Time = int(args.dbrefresh)
        except:
            print('[!] --dbrefresh requires integer')
            sys.exit()
            
    if args.initscreen:
        # NOTE: if SSH: Screen = 1; if App: Screen = 0;
        try:
            screen_as_integer = int(args.initscreen)
            if int(screen_as_integer) not in [0, 1]:
                print('[!] --initscreen requires 0 or 1')
                sys.exit()
            Screen = int(args.initscreen)
        except:
            print('[!] --initscreen requires 0 or 1')
            sys.exit()
    if not args.initscreen:
        Screen = 0

    if args.initdb:
        if not os.path.isfile('UNSPEAKABLEEVIL.db'):
            pass
            #print('[+] Database already exists at ./UNSPEAKABLEEVIL.db')
        elif not os.path.isfile('UNSPEAKABLEEVIL.db'):
            Init_DB()

    if args.schedule:
        print('[!] Unfinished')
        sys.exit()
        
        schedule_args = args.schedule.split(' ')
        
        Task_Name = schedule_args[0]
        Task_Run_Every_S = schedule_args[::-1][2]
        Task_Run_Wake_Every_S = schedule_args[::-1][1]
        Task_Active = schedule_args[::-1][0]
        
        # This cannot gaurantee filename if it has space characters
        # TODO: del(schedule_args[index]) of known variables and then join remaining to string for eg path+file name.sh
        Task_PathFile = ''
        
        
        conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
        c = conn.cursor()
        c.execute("INSERT INTO Scripts VALUES (\'%s\',\'%s\',%d,%d,%d,%d,%d)" % (Task_Name, Task_PathFile, int(Task_Run_Every_S), int(Task_Run_Wake_Every_S), Epoch_Time-13600, Epoch_Time-13600, int(Task_Active)))
        conn.commit() # Commit changes
        conn.close() # Close connection
        sys.exit()
    
    elif args.interactiveschedule:
        try:
            Task_Name = input('\n[+] Enter task name (eg Super_Villian_News_Checker)\n[NAME] ')
            Task_PathFile = input('\n[+] Enter path/filename (eg /home/nemo/Documents/test.sh)\n[PATHFILE] ')
        except:
            print('[!] Got exception\n')
            sys.exit()
        try:
            Task_Run_Every_S = int(input('\n[+] Enter every X seconds to run task, when screen is on. (eg 3600)\n[SECONDS] '))
            Task_Run_Wake_Every_S = int(input('\n[+] Enter every X seconds to run task, when user unlocks screen. (eg 3600 for every hour or 1 for everytime device is unlocked)\n[SECONDS] '))
        except:
            print('[!] Got exception: Enter integer value')
            sys.exit()
        try:
            Task_Active = input('\n[+] Activate script now? (Default=Y)\n[Yy/Nn] ')
            if Task_Active.upper()[0] == 'Y':
                Task_Active = 1
            else:
                print('[+] Task will be set to inactive')
                Task_Active = 0
        except:
            Task_Active = 1
        try:
            conn = sqlite3.connect('UNSPEAKABLEEVIL.db')
            c = conn.cursor()
            c.execute("INSERT INTO Scripts VALUES (\'%s\',\'%s\',%d,%d,%d,%d,%d)" % (Task_Name, Task_PathFile, int(Task_Run_Every_S), int(Task_Run_Wake_Every_S), Epoch_Time-13600, Epoch_Time-13600, int(Task_Active)))
            conn.commit() # Commit changes
            conn.close() # Close connection
            print('\n[+] Scheduled script successfully created!')
        except:
            print('[!] Got exception\n')
            sys.exit()

    elif args.test:
        c.execute("INSERT INTO Scripts VALUES ('TESTDEVC','/home/nemo/Documents/test.sh',30,1,%d,%d,1)" % (Epoch_Time-13600, Epoch_Time-13600))
        print('[+] Test script added to schedule [/home/nemo/Documents/test.sh]')
        print('[+] Run EVIL-DOER.py --scripts for more information')
    
    elif args.testnotification:
        import dbus

        bus = dbus.SessionBus()
        object = bus.get_object('org.freedesktop.Notifications','/org/freedesktop/Notifications')
        interface = dbus.Interface(object,'org.freedesktop.Notifications')
        
        interface.Notify('app_name',
                 0,
                 'icon-m-notifications',
                 'Test title',
                 '<u>Test Body</u>\nLine 2 test',
                 dbus.Array(['default', '']),
                 dbus.Dictionary({'x-nemo-preview-body': 'Test Body Preview',
                                  'x-nemo-preview-summary': 'Test Preview Summary'},
                                  signature='sv'),
                 0)
        sys.exit()
    
    elif args.removescript:
        Delete_Script_From_DB(args.removescript)
             
    elif args.scripts:
        print('[+] Checking database for scripts to run...')
        Result = Get_Scripts_From_DB()
        if len(Result) == 0:
            print('[!] There are no scripts in the database')
        elif len(Result) > 0:
            print('[+] Found %d scripts in database' % len(Result))
            for script in Result:
                Skip = False
                Script_Name = script[0]
                
                if not args.scripts == '*':
                    # Looking for a specific script
                    if not args.scripts.upper() == Script_Name.upper():
                        Skip = True # Well skip this one since were looking for specific script
                        
                if Skip == False:
                    Script_Pathfile = script[1]
                    Run_Every = script[2]
                    Run_Every_Wake = script[3]
                    Epoch_Last_Ran = script[4]
                    Epoch_Last_Wake_Run = script[5]
                    Script_Active = script[6]
                    
                    print('\n[Script Name]    %s' % str(Script_Name))
                    print('[Path + File]    %s' % str(Script_Pathfile))
                    if int(Script_Active) == 1:
                        print('[Script_Active]  True')
                    elif int(Script_Active) == 0:
                        print('[Script_Active]  False')
                    print('[Description]')
                    print(' * Script will run every %s seconds if screen is on' % str(Run_Every))
                    print(' * Script will run every %s seconds when user unlocks device' % str(Run_Every_Wake))
        print('\n')
        sys.exit()
    
    MOTD()

    if not os.path.isfile('UNSPEAKABLEEVIL.db'):
        Init_DB()
    
    # Start thread to listen for dbus unlock message
    Start_Thread(DBUS_Monitor)
    
    print(Screen)

    for i in range(0, 100):
        
        if Screen == 1:
            
            print('[+] Checking database for scripts to run...')
            Result = Get_Scripts_From_DB()
            #print(Result)
            
            if len(Result) == 0:
                print('[!] There are no scripts to run')
            
            elif len(Result) > 0:
                
                for script in Result:
                    print('[+] Checking:\n%s\n\n' % str(script))
                    # Check last time ran, do we need to run the file?
                    Script_Name = script[0]
                    Script_Pathfile = script[1]
                    Run_Every = script[2]
                    Run_Every_Wake = script[3]
                    Epoch_Last_Ran = script[4]
                    Epoch_Last_Wake_Run = script[5]
                    Script_Active = script[6]
                    
                    # Get current Epoch time
                    Epoch_Time = int(time.time())
                    #print(Epoch_Last_Ran + Run_Every)
                    #print(Epoch_Time)
                    
                    if Epoch_Last_Ran + Run_Every < Epoch_Time and Screen == 1 and Script_Active == 1:
                        # Command = ['sh', '/home/nemo/Documents/test.sh']
                        #if Script_Pathfile[::-1][0:3][::-1].lower() == '.py':
                        #    Interpretter = 'python3'
                        #else:
                        Interpretter = 'sh'
                        Command = [Interpretter, Script_Pathfile]
                        print('[+] Running %s' % Script_Name)
                        call(Command)
                        
                        # Update database
                        Update_DB_Last_Runtime(Script_Name)

            print('[+] Sleeping for %d Seconds' % DB_Refresh_Time)
            time.sleep(DB_Refresh_Time)
    
